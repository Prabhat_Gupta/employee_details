# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* To store the details of all the employees of an organisation
* Version 1.0

### How do I get set up? ###

* Summary of set up : First we need to install the jdk version 1.8 and Eclipse IDE Oxygen 4.7 and Gradle 4.1 version. After that, we need to add Buildship Gradle Integration 2.0 extension in Eclipse IDE . Now we need to write the build script in Groovy to build our project as Gradle project.We need to add the following dependencies in our build script 
    dependencies {
    compile 'org.restlet.jee:org.restlet:2.3.1'
    compile group: 'com.fasterxml.jackson.core', name: 'jackson-annotations', version: '2.5.4'
    compile group: 'org.restlet.jee', name: 'org.restlet.ext.jackson', version: '2.3.3'
   }
   Now, build the project by typing "gradle build "(where 'build.gradle' is the name of build script file) command in cmd.
   Now, the Project has been build .
   Now, We can start the server .
   
* Prerequisites : 
  1. RESTlet framework(JAVA)
  2. Gradle 4.1 version
  2. Groovy (to write the build script for Gradle tool)
  3. Bootstrap
  4. Jquery  

* Database configuration : Inbuild virtual Database (HashMap)

### Who do I talk to? ###

* Prabhat Gupta (prabhat@surewaves.com)