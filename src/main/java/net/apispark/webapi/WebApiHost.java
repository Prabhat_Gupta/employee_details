package net.apispark.webapi;

import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Restlet;
import org.restlet.data.Protocol;
import org.restlet.engine.Engine;
import org.restlet.ext.jackson.JacksonConverter;
import org.restlet.resource.Directory ;
 
public class WebApiHost extends Restlet {
	
    public static void main(String[] args) throws Exception {

    	Engine.getInstance().getRegisteredConverters().add(new JacksonConverter());
        // Attach application to http://localhost:9001/v1
        Component c = new Component();
        c.getServers().add(Protocol.HTTP, 9001);
        c.getClients().add(Protocol.FILE); 
        WebApiApplication application = new WebApiApplication();
        c.getDefaultHost().attach("/v1",application);
         
        c.start();
    }
}