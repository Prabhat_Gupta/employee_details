package net.apispark.webapi.resource.server;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.restlet.data.Reference;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.data.Preference;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.Header;
import org.restlet.engine.header.HeaderConstants;
//import org.restlet.ext.jackson.JacksonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.EmptyRepresentation;
import org.restlet.representation.ObjectRepresentation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.restlet.util.Series;
import java.util.logging.Level;

import net.apispark.webapi.representation.AddressJson;
import net.apispark.webapi.representation.CompanyJson;
import net.apispark.webapi.representation.EmployeesEmployeeidRepresentation;
import net.apispark.webapi.representation.ProjectJson;
import net.apispark.webapi.resource.Employee;
import net.apispark.webapi.resource.EmployeesEmployeeidResource;
import net.apispark.webapi.utils.QueryParameterUtils;
import net.apispark.webapi.utils.PathVariableUtils;
import java.util.HashMap ;
import net.apispark.webapi.resource.Employee;
import net.apispark.webapi.resource.StoreData;
public class EmployeesEmployeeidServerResource extends AbstractServerResource implements EmployeesEmployeeidResource {

	
	 
    // Define allowed roles for the method "get".
    private static final String[] get20AllowedGroups = new String[] {"anyone"};
    // Define denied roles for the method "get".
    private static final String[] get20DeniedGroups = new String[] {};

    public net.apispark.webapi.representation.EmployeesEmployeeidRepresentation represent() throws Exception {
       net.apispark.webapi.representation.EmployeesEmployeeidRepresentation result = null;
        checkGroups(get20AllowedGroups, get20DeniedGroups);
        

        try {
		
			// Path variables
			
	    String employeeidPathVariable = Reference.decode(getAttribute("employeeid"));
        //* PathVariableUtils.checkInteger(customeridPathVariable);
        // Query parameters
        
        	
	    result = new net.apispark.webapi.representation.EmployeesEmployeeidRepresentation();
	    
	    // Initialize here your bean
	    result = StoreData.hashmap.get(employeeidPathVariable);
	   
  //  result.id =  hm.size();
//        result.firstname = employeeidPathVariable ;
//	    result.lastName = "ndnednf" ;
//	    ProjectJson[] project = new ProjectJson[2];
//	    project[0] = new ProjectJson();
//	    project[1] =  new ProjectJson();
//	    project[0].id = 1 ;
//	    project[0].name = "dfscds" ;
//	    project[0].client = "fvdvgd";
//	    project[1].id = 1 ;
//	    project[1].name = "xvzfgs" ;
//	    project[1].client = "vfdsgv";
//	    result.project = project ;
//	    CompanyJson company = new CompanyJson();
//	    company.id = 1 ;
//	    company.name = "vfscs" ;
//	    company.headquarters = "vsdfccvc" ;
//	    company.product = "vsdfsdc" ;
//	    result.company = company ;
//	    result.designation = "vzsfc" ;
//	    AddressJson address = new AddressJson();
//	    address.id = 1 ;
//	    address.streetno = 1 ;
//	    address.landmark = "vcdfsvfs" ;
//	    address.pincode = 1 ;
//	    address.state = "dasfca" ;
//	    address.country = "Fxcadss" ;
//	    result.address = address;
	    
         } catch (Exception ex) {
            // In a real code, customize handling for each type of exception
            getLogger().log(Level.WARNING, "Error when executing the method", ex);
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL,
                    ex.getMessage(), ex);
        }
       
        return result;
    }

    // Define allowed roles for the method "put".
    private static final String[] put21AllowedGroups = new String[] {"anyone"};
    // Define denied roles for the method "put".
    private static final String[] put21DeniedGroups = new String[] {};

    public net.apispark.webapi.representation.EmployeesEmployeeidRepresentation store(Employee employee) throws Exception {
       net.apispark.webapi.representation.EmployeesEmployeeidRepresentation result = null;
        checkGroups(put21AllowedGroups, put21DeniedGroups);
        

        try {
		
			// Path variables
			
	    String employeeidPathVariable = Reference.decode(getAttribute("employeeid"));

        // Query parameters
        
	    result = new net.apispark.webapi.representation.EmployeesEmployeeidRepresentation();
	   
	    
	    // Initialize here your bean
	    if(StoreData.hashmap.containsKey(employeeidPathVariable))
	    {
//	    	for (Field f : employee.getClass().getFields()) {
//	    		  f.setAccessible(true);
//	    		  if (f.get(employee) == null) {
//	    		     throw new Exception();
//	    		  }
//	    		}
	    StoreData.hashmap.put(employeeidPathVariable,employee) ;
	    result = StoreData.hashmap.get(employeeidPathVariable) ;
	    }
//	    result.id = hm.size();
//      result.firstname = employee.firstname ;
//	    result.lastName = employee.lastName ;
//	    ProjectJson[] project = new ProjectJson[2];
//	    project[0] = new ProjectJson();
//	    project[1] =  new ProjectJson();
//	    project[0].id = employee.project[0].id ;
//	    project[0].name = employee.project[0].name ;
//	    project[0].client = employee.project[0].client;
//	    project[1].id = employee.project[1].id ;
//	    project[1].name = employee.project[1].name ;
//	    project[1].client = employee.project[1].client;
//        result.project = project ;
//        CompanyJson company = new CompanyJson();
//	    company.id = employee.company.id ;
//	    company.name = employee.company.name ;
//	    company.headquarters = employee.company.headquarters ;
//	    company.product = employee.company.product ;
//        result.company = employee.company;
//        result.designation = employee.designation ;
//        AddressJson address = new AddressJson();
//	    address.id = employee.address.id ;
//	    address.streetno = employee.address.streetno ;
//	    address.landmark = employee.address.landmark ;
//	    address.pincode = employee.address.pincode ;
//	    address.state = employee.address.state ;
//	    address.country = employee.address.country ;
//	    result.address = address;
         } catch (Exception ex) {
            // In a real code, customize handling for each type of exception
            getLogger().log(Level.WARNING, "Error when executing the method", ex);
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL,
                    ex.getMessage(), ex);
        }
    
        return result;
    }

    // Define allowed roles for the method "delete".
    private static final String[] delete22AllowedGroups = new String[] {"anyone"};
    // Define denied roles for the method "delete".
    private static final String[] delete22DeniedGroups = new String[] {};

    public net.apispark.webapi.representation.EmployeesEmployeeidRepresentation remove() throws Exception {
       net.apispark.webapi.representation.EmployeesEmployeeidRepresentation result = null;
        checkGroups(delete22AllowedGroups, delete22DeniedGroups);
        

        try {
		
			// Path variables
			
	    String employeeidPathVariable = Reference.decode(getAttribute("employeeid"));

        // Query parameters
        
        	
	    result = new net.apispark.webapi.representation.EmployeesEmployeeidRepresentation();
	    
	    // Initialize here your bean
	    result = StoreData.hashmap.get(employeeidPathVariable);
	    StoreData.hashmap.remove(employeeidPathVariable) ;
         } catch (Exception ex) {
            // In a real code, customize handling for each type of exception
            getLogger().log(Level.WARNING, "Error when executing the method", ex);
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL,
                    ex.getMessage(), ex);
        }
    
        return result;
    }
 
    // Define allowed roles for the method "patch".
    private static final String[] patch23AllowedGroups = new String[] {"anyone"};
    // Define denied roles for the method "patch".
    private static final String[] patch23DeniedGroups = new String[] {};

    public net.apispark.webapi.representation.EmployeesEmployeeidRepresentation partiallyChange(Employee employee) throws Exception {
       net.apispark.webapi.representation.EmployeesEmployeeidRepresentation result = null;
        checkGroups(patch23AllowedGroups, patch23DeniedGroups);
        

        try {
		
			// Path variables
			
	    String employeeidPathVariable = Reference.decode(getAttribute("employeeid"));

        // Query parameters
        
        	
	    result = new net.apispark.webapi.representation.EmployeesEmployeeidRepresentation();
	    
	    // Initialize here your bean
	   if(StoreData.hashmap.containsKey(employeeidPathVariable))
	   {
		Employee employee_patch = StoreData.hashmap.get(employeeidPathVariable);
		    if(employee.id != null)
		    	employee_patch.id = employee.id;
		    if(employee.firstname != null && employee.firstname != "" )
		    	employee_patch.firstname = employee.firstname ;
		    if(employee.lastName != null && employee.lastName != "")
		    	employee_patch.lastName = employee.lastName ;
		    if(employee.project != null)
		    {
            for(int i = 0 ; employee.project.length != 0 && i < employee_patch.project.length; i++)
              {
            	if(employee.project[i].id != null)
            		employee_patch.project[i].id = employee.project[i].id;
            	if(employee.project[i].name != null)
            		employee_patch.project[i].name = employee.project[i].name;
            	if(employee.project[i].client != null)
            		employee_patch.project[i].client = employee.project[i].client;		
              }
		    }
		    if(employee.company != null)
		    {
             if(employee.company.id != null)
            	 employee_patch.company.id = employee.company.id ;
             if(employee.company.name != null && employee.company.name != "")
            	 employee_patch.company.name = employee.company.name ;
             if(employee.company.product != null && employee.company.product != "")
            	 employee_patch.company.product = employee.company.product ;
             if(employee.company.headquarters != null && employee.company.headquarters != "")
            	 employee_patch.company.headquarters = employee.company.headquarters ;
		    }
             if(employee.designation != null && employee.designation != "")
            	 employee_patch.designation = employee.designation;
             if(employee.address != null)
             {
             if(employee.address.id != null)
            	 employee_patch.address.id = employee.address.id ;
             if(employee.address.streetno != null)
            	 employee_patch.address.streetno = employee.address.streetno ;
             if(employee.address.landmark != null && employee.address.landmark != "")
            	 employee_patch.address.landmark = employee.address.landmark ;
             if(employee.address.pincode != null)
            	 employee_patch.address.pincode = employee.address.pincode ;
             if(employee.address.state != null && employee.address.state != "")
            	 employee_patch.address.state = employee.address.state ;
             if(employee.address.country != null && employee.address.country != "")
            	 employee_patch.address.country = employee.address.country ;
             }
             
             StoreData.hashmap.put(employeeidPathVariable,employee_patch) ;
             result = StoreData.hashmap.get(employeeidPathVariable);
      	   
	   }
	    
         } catch (Exception ex) {
            // In a real code, customize handling for each type of exception
            getLogger().log(Level.WARNING, "Error when executing the method", ex);
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL,
                    ex.getMessage(), ex);
        }
    
        return result;
    }

}

