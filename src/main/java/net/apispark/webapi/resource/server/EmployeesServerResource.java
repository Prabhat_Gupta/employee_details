package net.apispark.webapi.resource.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.restlet.data.Reference;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.data.Preference;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.Header;
import org.restlet.engine.header.HeaderConstants;
import org.restlet.ext.jackson.JacksonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.EmptyRepresentation;
import org.restlet.representation.ObjectRepresentation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.restlet.util.Series;
import java.util.logging.Level;

import net.apispark.webapi.resource.Employee;
import net.apispark.webapi.resource.EmployeesResource;
import net.apispark.webapi.resource.StoreData;
import net.apispark.webapi.utils.QueryParameterUtils;
import net.apispark.webapi.utils.PathVariableUtils;


public class EmployeesServerResource extends AbstractServerResource implements EmployeesResource {

	
	 
    // Define allowed roles for the method "get".
    private static final String[] get17AllowedGroups = new String[] {"anyone"};
    // Define denied roles for the method "get".
    private static final String[] get17DeniedGroups = new String[] {};

    public Object[] represent() throws Exception {
      Object[] result = null;
        checkGroups(get17AllowedGroups, get17DeniedGroups);
        

        try {
		
        // Query parameters
        
        	
	   // result = new net.apispark.webapi.representation.EmployeesRepresentation();
	    
	    // Initialize here your bean
	   Collection set=  StoreData.hashmap.values();
	   Object[] arr = set.toArray();
	   result = arr ;
         } catch (Exception ex) {
            // In a real code, customize handling for each type of exception
            getLogger().log(Level.WARNING, "Error when executing the method", ex);
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL,
                    ex.getMessage(), ex);
        }
    
        return result;
    }

    // Define allowed roles for the method "post".
    private static final String[] post18AllowedGroups = new String[] {"anyone"};
    // Define denied roles for the method "post".
    private static final String[] post18DeniedGroups = new String[] {};

    public void add(Employee employee) throws Exception {
      // net.apispark.webapi.representation.EmployeesRepresentation result = null;
        checkGroups(post18AllowedGroups, post18DeniedGroups);
        

        try {
		
        // Query parameters
        
        	
	   // result = new net.apispark.webapi.representation.EmployeesRepresentation();
	    
	    // Initialize here your bean
	    String employeeidPathVariable = employee.id + "";
	    StoreData.hashmap.put(employeeidPathVariable,employee) ;
	    
	    
         } catch (Exception ex) {
            // In a real code, customize handling for each type of exception
            getLogger().log(Level.WARNING, "Error when executing the method", ex);
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL,
                    ex.getMessage(), ex);
        }
      //  return result;
    }

    // Define allowed roles for the method "delete".
    private static final String[] delete19AllowedGroups = new String[] {"anyone"};
    // Define denied roles for the method "delete".
    private static final String[] delete19DeniedGroups = new String[] {};

    public void remove() throws Exception {
        checkGroups(delete19AllowedGroups, delete19DeniedGroups);
        

        try {
        	
        	 StoreData.hashmap.clear();
         } catch (Exception ex) {
            // In a real code, customize handling for each type of exception
            getLogger().log(Level.WARNING, "Error when executing the method", ex);
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL,
                    ex.getMessage(), ex);
        }
    
        
    }


}

