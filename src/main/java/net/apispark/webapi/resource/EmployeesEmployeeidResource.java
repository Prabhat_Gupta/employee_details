package net.apispark.webapi.resource;

import org.restlet.resource.Get;
import org.restlet.resource.Put;
import org.restlet.resource.Delete;
import org.restlet.resource.Options;
import org.restlet.resource.Patch;
import org.restlet.representation.Representation;
public interface EmployeesEmployeeidResource {

    @Get
    net.apispark.webapi.representation.EmployeesEmployeeidRepresentation represent() throws Exception;

    @Put
     net.apispark.webapi.representation.EmployeesEmployeeidRepresentation store(Employee employee) throws Exception;
     
    @Delete
    net.apispark.webapi.representation.EmployeesEmployeeidRepresentation remove() throws Exception;
    
    @Patch
     net.apispark.webapi.representation.EmployeesEmployeeidRepresentation partiallyChange(Employee employee) throws Exception;
   
    

}
