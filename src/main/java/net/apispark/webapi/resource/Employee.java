package net.apispark.webapi.resource;

import java.io.Serializable;

import net.apispark.webapi.representation.AddressJson;
import net.apispark.webapi.representation.CompanyJson;
import net.apispark.webapi.representation.EmployeesEmployeeidRepresentation;
import net.apispark.webapi.representation.ProjectJson;

public class Employee extends EmployeesEmployeeidRepresentation {

	private static final long serialVersionUID = 1L;

	public Integer id;
	public String firstname;
	public String lastName;
	public ProjectJson[] project;
	public CompanyJson company;
	public String designation;
	public AddressJson address;
}
