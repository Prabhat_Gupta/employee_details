package net.apispark.webapi.representation;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class EmployeesEmployeeidRepresentation {
    /** Default serial version ID. */
    private static final long serialVersionUID = 1L;
    
    public int id  ;
    public String firstname ;
    public String lastName ;
    public ProjectJson[] project ;
    public CompanyJson company ;
    public String designation ;
    public AddressJson address ;
}
